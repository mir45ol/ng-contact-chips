/*
 * Public API Surface of ng-contact-chips
 */

export * from './lib/components/contact-chips/contact-chips.component';
export * from './lib/models/contact.model';
export * from './lib/ng-contact-chips.module';
