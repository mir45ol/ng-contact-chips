export class Contact {
  image: string;
  firstName: string;
  lastName: string;
  emailAddress: string;
}
